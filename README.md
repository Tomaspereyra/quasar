# Operacion fuego de quasar

## Objetivo
-Obtener la ubicacion en un espacio bidimencional a partir de tres puntos y sus respectivas distancias.

-Decodificar mensajes que nos envian desde diferentes posiciones.


## Solucion propuesta

Para obtener la ubicacion se utilizo la solucion propuesta en https://github.com/lemmingapex/trilateration que logro resolver este problema con el algoritmo [Levenberg-Marquardt](http://en.wikipedia.org/wiki/Levenberg%E2%80%93Marquardt_algorithm) de [Apache Commons Math](http://commons.apache.org/proper/commons-math/). 

Para la decodificacion de mensajes se creo un algoritmo recursivo que itera sobre los mensajes entrantes hasta obtener el mensaje final (si es posible).


```java
public List<String> getMessage(List<ArrayList<String>> messages, List<String> msjDecode )throws BadRequestException{
        List<String> msgByColumn = new ArrayList<>();
        int sizeOfMinArrayMessage = messages.stream().min(Comparator.comparing(ArrayList::size)).get().size();
        cutLists(messages, sizeOfMinArrayMessage);
        messages.forEach(msgFromSatellite -> {

        String elementToAdd = msgFromSatellite.stream().findFirst().get();
        msgByColumn.add(elementToAdd);
        msgFromSatellite.remove(elementToAdd);

        });

        msjDecode.add(selectDuplicatedOrUniqueString(msgByColumn));

        if (sizeOfMinArrayMessage > 1)
        msjDecode = getMessage(messages, msjDecode);

        if(msjDecode.stream().anyMatch(String::isEmpty))
        throw new BadRequestException("Error: message can't decode");

        return msjDecode;
        }
```

## Stack utilizado

-Java con Spring boot

-Base de datos H2

## Arquitectura

El proyecto se estructuro siguiendo las bases de la arquitectura hexagonal

Flujo:
![img.png](img.png)


## Ejecucion

Como primer paso, debemos descargar el proyecto, luego tenemos dos opciones.


Opcion 1 : Usando mvnw

A- (Opcional) instalar maven wrapper.

Tutorial https://www.baeldung.com/maven-wrapper

Ejecutar

```bash
./mvnw spring-boot:run
```
Se desplegara en el puerto 8080

Opcion 2:


Usando docker

A- (Opcional) Si no tenes docker instalado, podes ver como hacerlo en https://docs.docker.com/get-docker/

Ejecutar:
```bash
docker build --tag quasar-app .
```
Luego

```bash
docker run -p 8080:8080 quasar-app
```

## Version de prueba
Se encuentra la ultima version deployada en heroku.

[URL](https://serene-eyrie-14884.herokuapp.com/topsecret_split/)

## Api documentacion

Se implemento swagger mediante la libreria SpringDoc. 

[URL Doc](https://serene-eyrie-14884.herokuapp.com/swagger-ui.html) 


