package com.meli.quasar;

import com.meli.quasar.drivers.exceptions.BadRequestException;
import com.meli.quasar.drivers.trilateration.DecoderService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
class DecodeMsgTests {


    @Test
    void decodeMsjTest()throws BadRequestException {
        DecoderService decoderService = new DecoderService();
        List<ArrayList<String>> messages = new ArrayList();
        messages.add(new ArrayList<String>(Arrays.asList("","este","es","un","mensaje")));
        messages.add(new ArrayList<String>(Arrays.asList("este","","un","mensaje")));
        messages.add(new ArrayList<String>(Arrays.asList("","","es","","mensaje")));
        List<String> message = decoderService.getMessage(messages,new ArrayList<String>());
        Assert.isTrue("este es un mensaje".equals(String.join(" ", message)),"No son iguales");
    }
    @Test
    void decodeMsjTest1()throws BadRequestException{
        DecoderService decoderService = new DecoderService();
        List<ArrayList<String>> messages = new ArrayList();
        messages.add(new ArrayList<String>(Arrays.asList("este","","","mensaje","")));
        messages.add(new ArrayList<String>(Arrays.asList("","es","","","secreto")));
        messages.add(new ArrayList<String>(Arrays.asList("este","","un","","")));
        List<String> message = decoderService.getMessage(messages,new ArrayList<String>());
        Assert.isTrue("este es un mensaje secreto".equals(String.join(" ",message)),"No son iguales");
    }

}
