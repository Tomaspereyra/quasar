package com.meli.quasar;

import com.meli.quasar.drivers.exceptions.BadRequestException;
import com.meli.quasar.drivers.trilateration.NonLinearTrilaterationSolver;
import com.meli.quasar.entities.dto.Location;
import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.nio.file.attribute.PosixFileAttributes;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class TrilaterationTests {

    @Test
    public void getPositionTest() throws BadRequestException {
        NonLinearTrilaterationSolver solver = new NonLinearTrilaterationSolver();
        List<Float> distances = new ArrayList<>();
        distances.add(8.06f);
        distances.add(13.97f);
        distances.add(23.32f);
        List<Location> positions = new ArrayList<>();
        positions.add(new Location(5.0f, -6.0f));
        positions.add(new Location(13.0f, -15.0f));
        positions.add(new Location(21.0f, -3.0f));
        Location location = solver.getLocation(distances, positions);
        Assert.isTrue(location.getX() != 0.0f,"X is null");
        Assert.isTrue(location.getY() != 0.0f,"y is null");


    }
}
