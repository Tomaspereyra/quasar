package com.meli.quasar.interactors;

import com.meli.quasar.adapters.decoder.IDecoderService;
import com.meli.quasar.adapters.repositories.ISateliteRepository;
import com.meli.quasar.adapters.trilateration.ITrilaterationService;

import com.meli.quasar.drivers.exceptions.BadRequestException;
import com.meli.quasar.entities.Satelite;
import com.meli.quasar.entities.dto.Location;
import com.meli.quasar.entities.dto.ResponseDTO;
import com.meli.quasar.entities.dto.SateliteDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DecoderInteractor {

    private ITrilaterationService trilaterationService;

    private IDecoderService decoderService;

    private ISateliteRepository sateliteRepository;

    public DecoderInteractor(ITrilaterationService trilaterationService, IDecoderService decoderService, ISateliteRepository sateliteRepository) {
        this.trilaterationService = trilaterationService;
        this.decoderService = decoderService;
        this.sateliteRepository = sateliteRepository;
    }

    public ResponseDTO processMessages(List<SateliteDTO> satelites) throws BadRequestException {
        if(satelites.size() < 3 || satelites.stream().anyMatch(satelite -> satelite.getMessage() == null))
            throw new BadRequestException("Error insufficient satellites or one message is empty");
        return decodeMessageAndGetLocation(satelites);

    }

    public ResponseDTO getMessageAndLocation() throws BadRequestException {
        List<Satelite> satelites = sateliteRepository.findAll();
        if(satelites.size() < 3 || satelites.stream().anyMatch(satelite -> satelite.getMessage() == null))
            throw new BadRequestException("Error insufficient satellites or one message is empty");
        List<SateliteDTO> satelitesDto = new ArrayList<>();
        satelites.forEach(satelite -> satelitesDto.add(new SateliteDTO(satelite.getName(), satelite.getDistance(), new ArrayList<>(satelite.getMessage()))));
        return decodeMessageAndGetLocation(satelitesDto);

    }




    private ResponseDTO decodeMessageAndGetLocation(List<SateliteDTO> satelites) throws BadRequestException {
        List<String> messages = decoderService.getMessage(satelites.stream().map(SateliteDTO::getMessage).collect(Collectors.toList()), new ArrayList<>());

        List<Float> distances = satelites.stream().map(SateliteDTO::getDistance).collect(Collectors.toList());
        List<Location> postions = findSatellites(satelites).stream().map(Satelite::getPosition).collect(Collectors.toList());
        Location location = trilaterationService.getLocation(distances, postions);

        return new ResponseDTO(location, String.join(" ", messages));
    }

    private List<Satelite> findSatellites(List<SateliteDTO> satellites) throws BadRequestException {
        List<Satelite> satelitesFromDB = new ArrayList<>();
        satellites.forEach(sateliteDTO -> {
                    Optional<Satelite> satelite = sateliteRepository.findSateliteByName(sateliteDTO.getName());
                    satelite.ifPresent(satelitesFromDB::add);
                }

        );
        if (satelitesFromDB.size() < 3)
            throw new BadRequestException("Error insufficient satellite");

        return satelitesFromDB;

    }


}
