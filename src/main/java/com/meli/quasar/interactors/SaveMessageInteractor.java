package com.meli.quasar.interactors;

import com.meli.quasar.adapters.repositories.ISateliteRepository;
import com.meli.quasar.drivers.exceptions.BadRequestException;
import com.meli.quasar.entities.Satelite;
import com.meli.quasar.entities.dto.SateliteDTO;

import java.util.Optional;

public class SaveMessageInteractor {

    private ISateliteRepository sateliteRepository;

    public SaveMessageInteractor(ISateliteRepository sateliteRepository) {
        this.sateliteRepository = sateliteRepository;
    }

    public void saveMessage(SateliteDTO satelite) throws BadRequestException {
        Optional<Satelite> sateliteFromDb = sateliteRepository.findSateliteByName(satelite.getName());

        if (sateliteFromDb.isEmpty())
            throw new BadRequestException("Bad request: name not found");

        sateliteFromDb.get().setMessage(satelite.getMessage());
        sateliteFromDb.get().setDistance(satelite.getDistance());
        sateliteRepository.save(sateliteFromDb.get());


    }
}
