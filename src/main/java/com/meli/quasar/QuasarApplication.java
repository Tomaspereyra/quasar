package com.meli.quasar;

import com.meli.quasar.drivers.repositories.JPASateliteRepository;
import com.meli.quasar.drivers.repositories.SQLSateliteRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.meli.quasar.drivers")
public class QuasarApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuasarApplication.class, args);
    }


}
