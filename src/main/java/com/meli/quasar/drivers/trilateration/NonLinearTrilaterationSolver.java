package com.meli.quasar.drivers.trilateration;

import com.lemmingapex.trilateration.NonLinearLeastSquaresSolver;
import com.lemmingapex.trilateration.TrilaterationFunction;
import com.meli.quasar.adapters.trilateration.ITrilaterationService;
import com.meli.quasar.drivers.exceptions.BadRequestException;
import com.meli.quasar.entities.dto.Location;
import org.apache.commons.math3.fitting.leastsquares.LeastSquaresOptimizer;
import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;


@Service
public class NonLinearTrilaterationSolver implements ITrilaterationService {


    @Override
    public Location getLocation(List<Float> distances, List<Location> positions) throws BadRequestException {
        if (distances.stream().anyMatch(Objects::isNull) || positions.isEmpty())
            throw new BadRequestException("Error: Distances or positions is empty");

        NonLinearLeastSquaresSolver solver = new NonLinearLeastSquaresSolver(new TrilaterationFunction(locationListToDouble(positions), distances.stream().mapToDouble(Float::doubleValue).toArray()), new LevenbergMarquardtOptimizer());
        LeastSquaresOptimizer.Optimum optimum = solver.solve();


        double[] centroid = optimum.getPoint().toArray();


        return new Location((float) centroid[0], (float) centroid[1]);


    }

    private double[][] locationListToDouble(List<Location> positions) throws BadRequestException {
        double[][] positionDouble = null;
        try {
            positionDouble = new double[][]{{positions.get(0).getX(), positions.get(0).getY()}, {positions.get(1).getX(), positions.get(1).getY()}, {positions.get(2).getX(), positions.get(2).getY()}};
        } catch (Exception e) {
            throw new BadRequestException("Error invalid data");
        }

        return positionDouble;

    }
}
