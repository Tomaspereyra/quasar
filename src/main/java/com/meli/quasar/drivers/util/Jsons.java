package com.meli.quasar.drivers.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

public class Jsons {
	public static String asJsonString(final Object obj) {
		try {
		  var mapper = getObjectMapper();
      mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
      return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static  <T> T objectFromString(Class<T> aClass, String value) throws JsonProcessingException {
		return getObjectMapper().readValue(value, aClass);
	}

	public static ObjectMapper getObjectMapper() {
		return new ObjectMapper().registerModule(new ParameterNamesModule())
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
		.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true)
				.registerModule(new Jdk8Module())
				.registerModule(new JavaTimeModule());
	}


	public static  <T> T objectFromString(TypeReference<T> listTypeReference, String value) throws JsonProcessingException {
		return getObjectMapper().readValue(value, listTypeReference);
	}
}
