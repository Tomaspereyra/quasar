package com.meli.quasar.drivers.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.meli.quasar.drivers.util.Jsons;
import com.meli.quasar.entities.dto.Location;

import javax.persistence.AttributeConverter;


public class JsonConverter implements AttributeConverter<Location, String> {
    @Override
    public String convertToDatabaseColumn(Location location) {
        String locationJson = null;
        try {
            locationJson = Jsons.getObjectMapper().writeValueAsString(location);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return locationJson;
    }

    @Override
    public Location convertToEntityAttribute(String s) {
        Location location = null;
        try {
             location =  Jsons.objectFromString(Location.class,s);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return location;
    }
}
