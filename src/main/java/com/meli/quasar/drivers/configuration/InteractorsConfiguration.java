package com.meli.quasar.drivers.configuration;

import com.meli.quasar.adapters.decoder.IDecoderService;
import com.meli.quasar.adapters.repositories.ISateliteRepository;
import com.meli.quasar.adapters.trilateration.ITrilaterationService;
import com.meli.quasar.interactors.DecoderInteractor;
import com.meli.quasar.interactors.SaveMessageInteractor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InteractorsConfiguration {

    @Bean
    public DecoderInteractor getDecoderInteractor(ITrilaterationService trilaterationService, IDecoderService decoderService, ISateliteRepository sateliteRepository){
        return new DecoderInteractor(trilaterationService,decoderService,sateliteRepository);
    }
    @Bean
    public SaveMessageInteractor getSaveMessageInteractor(ISateliteRepository sateliteRepository){
        return new SaveMessageInteractor(sateliteRepository);
    }
}
