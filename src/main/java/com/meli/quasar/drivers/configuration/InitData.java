package com.meli.quasar.drivers.configuration;

import com.meli.quasar.drivers.repositories.JPASateliteRepository;
import com.meli.quasar.drivers.repositories.SQLSateliteRepository;
import com.meli.quasar.drivers.util.Jsons;
import com.meli.quasar.entities.Satelite;
import com.meli.quasar.entities.dto.Location;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class InitData implements ApplicationRunner {

    private JPASateliteRepository repository;

    public InitData(JPASateliteRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        repository.save(new Satelite("kenobi",new Location(-500,-200)));
        repository.save(new Satelite("skywalker",new Location(100,-100)));
        repository.save(new Satelite("sato",new Location(500,100)));

    }
}
