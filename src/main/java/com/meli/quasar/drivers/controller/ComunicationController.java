package com.meli.quasar.drivers.controller;

import com.meli.quasar.drivers.exceptions.BadRequestException;
import com.meli.quasar.entities.dto.ResponseDTO;
import com.meli.quasar.entities.dto.SateliteDTO;
import com.meli.quasar.entities.dto.SateliteListDTO;
import com.meli.quasar.interactors.DecoderInteractor;

import com.meli.quasar.interactors.SaveMessageInteractor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ComunicationController {

    private DecoderInteractor decoderInteractor;

    private SaveMessageInteractor saveMessageInteractor;

    public ComunicationController(DecoderInteractor decoderInteractor,SaveMessageInteractor saveMessageInteractor) {
        this.decoderInteractor = decoderInteractor;
        this.saveMessageInteractor = saveMessageInteractor;
    }

    @PostMapping("/topsecret")
    public ResponseDTO processMessages(@RequestBody SateliteListDTO satelites) throws BadRequestException {
        return decoderInteractor.processMessages(satelites.getSatellites());
    }

    @PostMapping("/topsecret_split/{satellite_name}")
    public ResponseEntity<String> saveMessage(@PathVariable(value = "satellite_name") String name, @RequestBody SateliteDTO sateliteDTO) throws BadRequestException {
        sateliteDTO.setName(name);
        saveMessageInteractor.saveMessage(sateliteDTO);
        return ResponseEntity.ok("Message saved");
    }


    @GetMapping("/topsecret_split/")
    public ResponseDTO decodeMessageAndGetDistance() throws BadRequestException {
        return decoderInteractor.getMessageAndLocation();

    }

}
