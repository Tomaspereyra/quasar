package com.meli.quasar.drivers.repositories;

import com.meli.quasar.entities.Satelite;
import com.meli.quasar.entities.dto.SateliteDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface JPASateliteRepository extends JpaRepository<Satelite,Integer> {

    Optional<Satelite> findByName(String name);



}
