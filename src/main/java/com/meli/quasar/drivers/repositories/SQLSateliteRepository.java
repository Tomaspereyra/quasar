package com.meli.quasar.drivers.repositories;

import com.meli.quasar.adapters.repositories.ISateliteRepository;
import com.meli.quasar.entities.Satelite;
import com.meli.quasar.entities.dto.SateliteDTO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class SQLSateliteRepository implements ISateliteRepository {

    private JPASateliteRepository repository;

    public SQLSateliteRepository(JPASateliteRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<Satelite> findSateliteByName(String name) {
        return repository.findByName(name);
    }
    @Override
    public Satelite save(Satelite s){
        return repository.save(s);
    }

    @Override
    public List<Satelite> findAll() {
        return repository.findAll();
    }
}
