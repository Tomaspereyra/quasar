package com.meli.quasar.adapters.decoder;

import com.meli.quasar.drivers.exceptions.BadRequestException;

import java.util.ArrayList;
import java.util.List;

public interface IDecoderService {


    List<String> getMessage(List<ArrayList<String>> messages, List<String> msjDecode) throws BadRequestException;
}
