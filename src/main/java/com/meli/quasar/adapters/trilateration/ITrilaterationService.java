package com.meli.quasar.adapters.trilateration;

import com.meli.quasar.drivers.exceptions.BadRequestException;
import com.meli.quasar.entities.dto.Location;

import java.util.List;

public interface ITrilaterationService {

    Location getLocation(List<Float> distances,List<Location> positions) throws BadRequestException;
}
