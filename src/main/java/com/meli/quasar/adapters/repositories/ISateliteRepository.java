package com.meli.quasar.adapters.repositories;

import com.meli.quasar.entities.Satelite;
import com.meli.quasar.entities.dto.SateliteDTO;

import java.util.List;
import java.util.Optional;

public interface ISateliteRepository {

    Optional<Satelite> findSateliteByName(String name);

    Satelite save(Satelite s);

    List<Satelite> findAll();
}
