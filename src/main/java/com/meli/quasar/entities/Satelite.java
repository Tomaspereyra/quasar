package com.meli.quasar.entities;


import com.meli.quasar.drivers.util.JsonConverter;
import com.meli.quasar.entities.dto.Location;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "satelites")
public class Satelite {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Convert(converter = JsonConverter.class)
    private Location position;
    private String message;
    private Float distance;




    public Satelite( String name, Location position) {
        this.name = name;
        this.position = position;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public List<String> getMessage() {
        return message != null ?  Arrays.stream(message.split(",")).map(s -> s.replace(" ","")).collect(Collectors.toList()): null;
    }

    public void setMessage(List<String> message) {
        this.message = String.join(", ",message);
    }

    public Satelite() {

    }

    public Satelite(Long id, String name, Location position) {
        this.id = id;
        this.name = name;
        this.position = position;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getPosition() {
        return position;
    }

    public void setPosition(Location position){


        this.position =position;
    }
}
