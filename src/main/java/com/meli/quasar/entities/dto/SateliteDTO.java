package com.meli.quasar.entities.dto;

import java.util.ArrayList;


public class SateliteDTO {
    private String name;
    private Float distance;
    private ArrayList<String> message;

    public SateliteDTO(String name, Float distance, ArrayList<String> message) {
        this.name = name;
        this.distance = distance;
        this.message = message;
    }

    public SateliteDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public ArrayList<String> getMessage() {
        return message;
    }

    public void setMessage(ArrayList<String> message) {
        this.message = message;
    }
}
