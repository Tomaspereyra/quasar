package com.meli.quasar.entities.dto;

import java.util.List;

public class SateliteListDTO {

    private List<SateliteDTO> satellites;

    public List<SateliteDTO> getSatellites() {
        return satellites;
    }

    public void setSatellites(List<SateliteDTO> satellites) {
        this.satellites = satellites;
    }
}
