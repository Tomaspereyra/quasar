package com.meli.quasar.entities.dto;

public class ResponseDTO {

    private Location position;
    private String message;

    public ResponseDTO(Location position, String message) {
        this.position = position;
        this.message = message;
    }

    public Location getPosition() {
        return position;
    }

    public void setPosition(Location position) {
        this.position = position;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ResponseDTO{" +
                "position=" + position +
                ", message='" + message + '\'' +
                '}';
    }
}
